# Felipe Finhane de Paula	

Hi! I'm your first Markdown file in **StackEdit**. If you want to learn about StackEdit, you can read me. If you want to play with Markdown, you can edit me. If you have finished with me, you can just create new files by opening the **file explorer** on the left corner of the navigation bar.

## Contacts

- felipefinhane@gmail.com
- [Linkedin](www.linkedin.com/in/felipefdepaula)
- [GitHub](https://github.com/felipefinhane)

## Knowledge

- Programing languages
	- PHP
	- JavaScript
	- Java
	- Sql
## Work experience

Synchronization is one of the biggest features of StackEdit. It enables you to synchronize any file in your workspace with other files stored in your **Google Drive**, your **Dropbox** and your **GitHub** accounts. This allows you to keep writing on other devices, collaborate with people you share the file with, integrate easily into your workflow... The synchronization mechanism takes place every minute in the background, downloading, merging, and uploading file modifications.

- Tecnologia Fox
	- Programador PHP 
	> To start syncing your workspace, just sign in with Google in the menu.

- Guia-se
	- Programador PHP
	- Lider Técnico
	> To start syncing your workspace, just sign in with Google in the menu.

- Plus IT (Anhanguera educacional)
	- Programador PHP
	> To start syncing your workspace, just sign in with Google in the menu.
	
- Ops Web
	- Analista Programador
	> To start syncing your workspace, just sign in with Google in the menu.
